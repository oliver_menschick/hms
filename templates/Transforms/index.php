<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transform[]|\Cake\Collection\CollectionInterface $transforms
 */
?>
<div class="transforms index content">
    <?= $this->Html->link(__('New Transform'), ['action' => 'add'], ['class' => 'button float-right']) ?>
    <h3><?= __('Transforms') ?></h3>
    <div class="table-responsive">
        <table>
            <thead>
                <tr>
                    <th><?= $this->Paginator->sort('id') ?></th>
                    <th><?= $this->Paginator->sort('input_number') ?></th>
                    <th><?= $this->Paginator->sort('palindrom') ?></th>
                    <th><?= $this->Paginator->sort('number_of_cylces') ?></th>
                    <th><?= $this->Paginator->sort('created') ?></th>
                    <th><?= $this->Paginator->sort('modified') ?></th>
                    <th class="actions"><?= __('Actions') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($transforms as $transform): ?>
                <tr>
                    <td><?= $this->Number->format($transform->id) ?></td>
                    <td><?= $this->Number->format($transform->input_number) ?></td>
                    <td><?= $this->Number->format($transform->palindrom) ?></td>
                    <td><?= $this->Number->format($transform->number_of_cylces) ?></td>
                    <td><?= h($transform->created) ?></td>
                    <td><?= h($transform->modified) ?></td>
                    <td class="actions">
                        <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $transform->id], ['confirm' => __('Are you sure you want to delete # {0}?', $transform->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
    </div>
</div>
