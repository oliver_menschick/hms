<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Transform $transform
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Transforms'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="transforms form content">
            <?= $this->Form->create($transform) ?>
            <fieldset>
                <legend><?= __('Add Transform') ?></legend>
                <?php
                    echo $this->Form->control('input_number');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Transfor')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
