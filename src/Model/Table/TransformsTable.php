<?php
declare(strict_types=1);

namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\Entity;
use Cake\Log\Log;

/**
 * Transforms Model
 *
 * @method \App\Model\Entity\Transform newEmptyEntity()
 * @method \App\Model\Entity\Transform newEntity(array $data, array $options = [])
 * @method \App\Model\Entity\Transform[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\Transform get($primaryKey, $options = [])
 * @method \App\Model\Entity\Transform findOrCreate($search, ?callable $callback = null, $options = [])
 * @method \App\Model\Entity\Transform patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\Transform[] patchEntities(iterable $entities, array $data, array $options = [])
 * @method \App\Model\Entity\Transform|false save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transform saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\Transform[]|\Cake\Datasource\ResultSetInterface|false saveMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transform[]|\Cake\Datasource\ResultSetInterface saveManyOrFail(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transform[]|\Cake\Datasource\ResultSetInterface|false deleteMany(iterable $entities, $options = [])
 * @method \App\Model\Entity\Transform[]|\Cake\Datasource\ResultSetInterface deleteManyOrFail(iterable $entities, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class TransformsTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config): void
    {
        parent::initialize($config);

        $this->setTable('transforms');
        $this->setDisplayField('id');
        $this->setPrimaryKey('id');

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator): Validator
    {
        $validator
            ->integer('id')
            ->allowEmptyString('id', null, 'create');

        $validator
            ->integer('input_number')
            ->requirePresence('input_number', 'create')
            ->notEmptyString('input_number');

        $validator
            ->integer('palindrom')
            ->requirePresence('palindrom', 'create')
            ->notEmptyString('palindrom');

        $validator
            ->integer('number_of_cylces')
            ->requirePresence('number_of_cylces', 'create')
            ->notEmptyString('number_of_cylces');

        return $validator;
    }

    public function createPalindromeAsTransformEntity(int $n): Entity
    {
        $number_of_cycles = 0;
        $palindrome = $this->palindrome($n, $number_of_cycles);
        return $this->newEntity([
            'input_number' => $n,
            'palindrom' => $palindrome,
            'number_of_cylces' => $number_of_cycles
        ]);
    }

    public function palindrome(int $n, int &$number_of_cycles = 0): int
    {
        if ($number_of_cycles == 0 AND
            ($n < 1 OR $n > 10000)) {
                throw new \RuntimeException("N must be between 1 and 10000 inclusive");
        }

        if ($n > 1000000000) {
            return -1;
        }

        if ($this->isPalindrome($n)) {
            return $n;
        }

        $n_reversed = $this->reverse($n);
        $number_of_cycles++;

        return $this->palindrome($n + $n_reversed, $number_of_cycles);
    }

    public function isPalindrome(int $n):bool
    {
        if ($n === 0) {
            return true;
        }

        if ($n < 0) {
            return false;
        }

        $length = $this->_getLength($n);
        $rightNumber = $n % 10;
        $leftNumber = (int) floor($n / 10 ** ($length - 1));

        if ($rightNumber == $leftNumber) {
            if ($length > 3) {
                return $this->isPalindrome($this->_getMiddlepart($n));
            } else {
                return true;
            }
        } else {
            return false;
        }
    }

    public function reverse(int $n): int
    {

        $n_reversed = 0;
        while ($n > 0) {
            $n_reversed *= 10;
            $n_reversed += ($n % 10);
            $n = (int) ($n/10);
        }
        return $n_reversed;
    }

    private function _getLength(int $n): int
    {
        return (int) floor(log10($n) + 1);
    }

    private function _getMiddlepart(int $n)
    {
        $length = $this->_getLength($n);
        if ($length < 3) {
            return 0;
            throw new \RuntimeException("This method can be only called with numbers that have 3 or more digits");
        }

        $n_cut_right = (int) floor($n / 10);
        $n_middlepart = $n_cut_right % 10 ** ($length - 2);

        return $n_middlepart;
    }

}
