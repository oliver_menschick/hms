<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Transform Entity
 *
 * @property int $id
 * @property int $input_number
 * @property int $palindrom
 * @property int $number_of_cylces
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime $modified
 */
class Transform extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'input_number' => true,
        'palindrom' => true,
        'number_of_cylces' => true,
        'created' => true,
        'modified' => true,
    ];
}
