<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Utility\Hash;

/**
 * Transforms Controller
 *
 * @property \App\Model\Table\TransformsTable $Transforms
 * @method \App\Model\Entity\Transform[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class TransformsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        $transforms = $this->paginate($this->Transforms);

        $this->set(compact('transforms'));
    }


    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $transform = $this->Transforms->newEmptyEntity();
        if ($this->request->is('post')) {
            $transform = $this->Transforms->createPalindromeAsTransformEntity((int) $this->request->getData('input_number'));
            if ($this->Transforms->save($transform)) {
                $this->Flash->success(__('The transform has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->success(__('The transform could not be saved.'));
        }
        $this->set(compact('transform'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Transform id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $transform = $this->Transforms->get($id);
        if ($this->Transforms->delete($transform)) {
            $this->Flash->success(__('The transform has been deleted.'));
        } else {
            $this->Flash->error(__('The transform could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
