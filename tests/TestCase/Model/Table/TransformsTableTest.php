<?php
declare(strict_types=1);

namespace App\Test\TestCase\Model\Table;

use App\Model\Table\TransformsTable;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\TransformsTable Test Case
 */
class TransformsTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\TransformsTable
     */
    protected $Transforms;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp(): void
    {
        parent::setUp();
        $config = $this->getTableLocator()->exists('Transforms') ? [] : ['className' => TransformsTable::class];
        $this->Transforms = $this->getTableLocator()->get('Transforms', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown(): void
    {
        unset($this->Transforms);

        parent::tearDown();
    }

    /**
     * Test isPalindrome method
     *
     * @return void
     * @uses \App\Model\Table\TransformsTable::palindrome()
     */
    public function testIsPalindrome(): void
    {
        $this->assertFalse($this->Transforms->isPalindrome(28));
        $this->assertTrue($this->Transforms->isPalindrome(11));
        $this->assertTrue($this->Transforms->isPalindrome(121));
        $this->assertTrue($this->Transforms->isPalindrome(34543));
        $this->assertFalse($this->Transforms->isPalindrome(23422));
    }

    /**
     * Test reverse method
     *
     * @return void
     * @uses \App\Model\Table\TransformsTable::reverse()
     */
    public function testReverse(): void
    {
        $this->assertEquals(11, $this->Transforms->reverse(11));
        $this->assertEquals(82, $this->Transforms->reverse(28));
        $this->assertEquals(15, $this->Transforms->reverse(51));
        $this->assertEquals(706, $this->Transforms->reverse(607));
        $this->assertEquals(4321, $this->Transforms->reverse(1234));
    }

    /**
     * Test palindrome method
     *
     * @return void
     * @uses \App\Model\Table\TransformsTable::palindrome()
     */
    public function testPalindromeSuccess(): void
    {
        $this->assertEquals(11, $this->Transforms->palindrome(11));
        $this->assertEquals(121, $this->Transforms->palindrome(28));
        $this->assertEquals(66, $this->Transforms->palindrome(51));
        $this->assertEquals(4444, $this->Transforms->palindrome(607));
    }

    /**
     * Test palindrome method
     *
     * @return void
     * @uses \App\Model\Table\TransformsTable::palindrome()
     */
    public function testPalindromeOutOfRange(): void
    {
        $this->assertEquals(-1, $this->Transforms->palindrome(196));
    }

    /**
     * Test palindrome method
     *
     * @return void
     * @uses \App\Model\Table\TransformsTable::palindrome()
     */
    public function testPalindromeException(): void
    {
        $this->expectException(\RuntimeException::class);
        $this->Transforms->palindrome(0);
        $this->Transforms->palindrome(10001);
    }
}
